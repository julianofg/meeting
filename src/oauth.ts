export const OAuthSettings = {
  appId: '27770686-e00b-4b72-a8bf-69929c8767ec',
  scopes: [
    "user.read",
    "calendars.read",
    "Calendars.ReadWrite",
    "OnlineMeetings.ReadWrite"
  ]
};
